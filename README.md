# COVID-19 Trusted Sources

**Projet Twitter - Alexandre MOURIEC - Romain GEFFROY**

Notre projet Twitter est basé sur *l'objectif* de **mettre en avant les tweets traitant du Coronavirus/COVID-19** mais seulement par des experts ou organismes reconnus, pour éviter la propagation de fake news. Pour cela, **nous nous sommes basés sur la liste d'experts et d'organismes réalisée par [Fortune Magazine](https://fortune.com/2020/03/14/coronavirus-updates-twitter-accounts-covid-19-news/)**.

L'objectif du projet est de **récupérer les tweets de ces personnes ou organismes présents dans cette liste** et de les afficher dans une interface claire et aérée qui permet aux gens de s'informer sans avoir à posséder de comptes Twitter pour suivre ces comptes.

## Démarrage

### Cloner le repo

```shell
git clone https://gitlab.com/mrcalexandre/projet-twitter.git
cd projet-twitter
```

### Installez les packages npm sur la partie backend

Naviguez dans le dossier `back`:

```
cd back
```

Installez les packages `npm` décrits dans le fichier `package.json`:

```shell
npm install
//Lancer ensuite la commande suivante pour lancer le backend
npm run servdev
```

Dans le cas où vous souhaitez partir de zero pour éviter les problèmes, lancez la commande suivante

```shell
rm -rf node_modules/ && rm -rf package-lock.json && npm install && npm start
```

La commande `npm run servdev` va lancer la commande `nodemon app.js`, va surveiller toute modification pour s'actualiser, et va fonctionner sur le port `4201`.

Vous pouvez arrêter le serveur avec la commande `Ctrl-C`.

### Installez les packages npm sur la partie frontend

Naviguez dans le dossier `front`:

```
cd front
```

Installez les packages `npm` décrits dans le fichier `package.json`:

```shell
npm install
ng serve
```

Dans le cas où vous souhaitez partir de zero pour éviter les problèmes, lancez la commande suivante

```shell
rm -rf node_modules/ && rm -rf package-lock.json && npm install && npm start
```

La commande `ng serve` va lancer la compilation TypeScript et des différents fichiers, va surveiller toute modification pour s'actualiser, et va fonctionner sur le port `4200`.

Vous pouvez arrêter le serveur avec la commande `Ctrl-C`.

Si besoin, voici un lien vers la documentaire Angular : https://angular.io/docs

En cas de problème lancez la commande: 

```shell
rm -rf node_modules/ && rm -rf package-lock.json && npm install && ng serve
```

------

Quand vous avez effectué ces 2 étapes, vous pouvez visiter http://localhost:4200/home et découvrir l'interface du projet qui récupère les tweets.

Pour toute question, nous sommes joignables sur Discord ou par email

 `alexandre.mouriec@etudiant.univ-rennes1.fr` et  `romain.geffroy@etudiant.univ-rennes1.fr`